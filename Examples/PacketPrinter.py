from MimV6 import PacketPrinter
from scapy.all import Ether, IPv6, TCP

printer = PacketPrinter()
printer.print(Ether()/IPv6()/TCP())